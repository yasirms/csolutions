# Container Solutions Assignment

The solution has been divided into two steps:
1. MySQL DB Deployment
2. Python App Deployment

The solution assumes that all files are in the same directory. The CI/CD pipeline has not been implemented for this solution.

<h2> MySql Deployment </h2>

For MySQL deployment, 

<h3>Step 1 - Create Username and Password for MySQL</h3>
Create base64 secrets to be used for MySQL

 File: base24-secrets.sh

```
!#bin/bash
echo -n 'root' > username.txt
echo 'password123' >password.txt
kubectl create secret generic mysql-secrets --from-file=username.txt --from-file=password.txt
```

<h3> Step 2 - Apply Created Secrets in Kubernetes Cluster</h3>

Apply secrets in Kubernetes, so that they can be used as environment variables in MySQL deployment.

File: secret.yml
```
apiVersion: v1
kind: Secret
metadata:
  name: mysql-pass
type: Opaque
data:
  username: cm9vdA==
  password: cGFzc3dvcmQxMjM=
```

<h3> Step 3: Create Persistent Volume Claims for Persistent Storage of MySQL Data </h3>

Since pods are ephameral, below kubernetes manifest files creates Persistent volume claims. This will be resued in next step - for storing database data permanently.

File: pvc.yml

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-data-disk
spec:
  accessModes:
   - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
```

<h3> Step 4: Create MySQL Deployment </h3>

The below kubernetes manifest Deploys MySQL and create service. The image used for MySQL is version 5.7. The standard MySQL port 3306 has been exposed. The persistent volume claimed created in the previous step has been mounted at "/var/lib/mysql". The Root password is set using enviroement Variable and Referring to secret created in Step 2. 

Lastely, a service is created to ensure high availability of the MySQL cluster.

File: mysql.yml

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql-deployment
  labels:
    app: mysql
spec:
  replicas: 3
  selector:
    matchLabels:
      app: mysql
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
      - name: mysql
        image: mysql:5.7
        ports:
         - containerPort: 3306
        volumeMounts:
          - mountPath: "/var/lib/mysql"
            subPath: "mysql"
            name: mysql-data
        env:
          - name: MYSQL_ROOT_PASSWORD
            valueFrom:
              secretKeyRef: 
                name: mysql-pass
                key: password
      volumes:
        - name: mysql-data
          persistentVolumeClaim:
            claimName: mysql-data-disk

---
apiVersion: v1
kind: Service
metadata:
  name: mysql-service
spec:
  selector:
    app: mysql
  ports:
  - protocol: TCP
    port: 3306
    targetPort: 3306
```

<h2> Creating Docker Container for Python App </h2>

<h3> Step 1: Copy the titanic.csv File to Working Directory</h3>

The very first step is to copy the titanic.csv file to current working directory. For this solution it is assumed that <b>titanic.cs</b> is in the current director.

<h3> Step 2: Create Python Program that Extracts Data from titanic.csv and inserts into MySql DB </h3>

Filename: importdata.py

The below python program imports CSV and mysql.connctor modules/libraris. It connects with the MySQL database using connection string. The Host parameter is set to hostname of MySQL service deployed above.

The SQL statement creates Database, table with all required attributes.

DictReader reads CSV file. For loop inserts reach row one by one to the Databse Table: passinfo.

```
import csv
import mysql.connector

conn = mysql.connector.connect(host="mysql-service.svc.cluster.local", port=3306, user="root", passwd="password123")
cursor = conn.cursor()

sql ='CREATE DATABASE IF NOT EXISTS csdb; DROP TABLE IF EXISTS `passinfo`; CREATE TABLE passinfo (Survived bool, Pclass int, Name varchar(255), Sex varchar(255), Age int, SSA int, PCA int, Fare float)'

cursor.execute(sql)


with open('./titanic.csv') as csvfile:
    reader = csv.DictReader(csvfile, delimiter = ',')
    for row in reader:
        conn = mysql.connector.connect(host="mysql-service.svc.cluster.local", port=3306, user="root", passwd="password123", db="csdb")
        sql_statement = "INSERT INTO passinfo(Survived,Pclass,Name,Sex,Age,SSA, PCA,Fare ) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
        cur = conn.cursor()
        cur.executemany(sql_statement,[(row['Survived'], row['Pclass'], row['Name'], row['Sex'], row['Age'], row['Siblings/Spouses Aboard'], row['Parents/Children Aboard'], row['Fare'])])
        conn.commit()

```

<h3> Step 3 : Creat Docker Build File </h3>

File: DockerFile

Docker file pulls python image and add the required files to image. It then install Falsk and mysql-connector. Lastly it executes the python program created in last step to insert data into the Databse.
```
FROM python
EXPOSE 5000
WORKDIR .
ADD titanic.csv .
ADD importdata.py .
RUN pip install Flask
RUN pip install mysql-connector
CMD python importdata.py
```

<h3> Step 4: Build and Push Docker Image to docker hub </h3>

Run follwing script on the shell to build and push Docker Image

```
docker login
# Enter your username and passowrd for Dockerhub
docker build --tag <tag> .
docker push <tag>
```

The same imagename will be used in Python app deployment file.

<h3> Step 5: Python App deployment </h3>

File: pyapp.yml

The pyapp.yml manifest created deployment for python app. It pulls in Docker image created in the last step and created deployment for the Python app.

```
apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: py-app
  labels:
    name: py-app
spec:
  replicas: 1
  selector:
    matchLabels:
      name: py-app
  template:
    metadata:
      name: py-app
      labels:
        name: py-app
    spec:
      containers:
        - name: import-csv-data
          image: yasirmehmood/titanic:new12
          ports:
            - containerPort: 5000
```

<h3> Step 6: Wrapper Script to for Complete Deployment </h3>

File: wrapper.sh

The below script will implement Python app and MySQL DB deployments. It creates the secrets, persistent volume claims, MySQL service deployment and Python Data import app. 

You needed to be loggedin to Docker Hub to be able to push images. 

```
!#/bin/bash
./base24-secrets.sh
docker login
docker build --tag yasirmehmood/titanic.new15 .
docker push yasirmehmood/titanic.new15
kubectl apply -f secret.yml
kubectl apply -f pvc.yml
kubectl apply -f mysql.yml
kubectl apply -f pyapp.yml
```




























