FROM python
EXPOSE 5000
WORKDIR .
ADD titanic.csv .
ADD importdata.py .
RUN pip install Flask
RUN pip install mysql-connector
CMD python importdata.py
